# RapidMiner docker

## Purpose
To provide RapidMiner in docker container

## Build

```
./build.sh
```

## Run

```
./run.sh
```

